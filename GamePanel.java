import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.applet.*;

public class GamePanel extends JPanel implements MouseListener
{
	public static final int SIZE = 64;//一マスの大きさ

	public static final int MASU = 8;//縦横

	public static final int WIDTH = SIZE * MASU;

	public static final int HEIGHT = WIDTH;

	private int[][] game = new int[MASU][MASU];

	public static final int BLACK = 1;//黒

	public static final int BLANK = 0;//置いてない時

	public static final int WHITE = -1;//白

	private boolean flagblack;

	public InfoPanel infopanel;

	private int[] direct;

	public static final int REVERSE_SLEEP = 500;
	public static final int TAP_SLEEP = 300;


	public GamePanel(InfoPanel infopanel)
	{
		this.infopanel = infopanel;
		initboard();
		flagblack = true;
		addMouseListener(this);
	}

	private void initboard()
	{
		for (int i = 0; i < MASU; i++) {
			for (int j = 0; j < MASU; j++) {
				game[i][j] = BLANK;
			}
		}

		game[3][3] = BLACK;
		game[4][4] = BLACK;
		game[3][4] = WHITE;
		game[4][3] = WHITE;
	}

	/*マウスがクリックされた時*/

	public void mouseClicked(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();

		int putX = x / SIZE;
		int putY = y / SIZE;

		System.out.println("X: " +putX+ "  Y: "+putY);

		putStone(putX, putY);
	}

	/*
	*配列に石を追加する関数
	*/

	public void putStone(int x, int y)
	{
		int stone;
		if(flagblack)
		{
			stone = BLACK;
		}else
		{
			stone = WHITE;
		}

		if(canPut(x,y,stone))
		{
			game[x][y] = stone;

			flagblack = !flagblack;//逆さにします

			sleep(TAP_SLEEP);

			infopanel.update(countBlack(), countWhite(), flagblack);

			reverse(x,y,stone);

			repaint();
		}
	}

	public boolean canPut(int x, int y, int stone)
	{
		if(game[x][y] != BLANK)
		{
			return false;
		}else
		{
			/*
			*0 上
			*1 右上
			*2 右
			*3 右下
			*4 下
			*5 左下
			*6 左
			*7 左上
			*/

			direct = new int[8];//マスの方向
			for (int i = 0; i < 8; i++) {
				direct[i] = 0;
			}

			int i = 1;
			if((y-i) >= 0)
			{
				if(game[x][y-i] == -stone) direct[0] = 1;
			}

			if((x+i) < MASU && (y-i) >= 0)
			{
				if(game[x+i][y-i] == -stone) direct[1] = 1;
			}

			if((x+i) < MASU)
			{
				if(game[x+i][y] == -stone) direct[2] = 1;
			}

			if((y+i) < MASU)
			{
				if(game[x][y+i] == -stone) direct[4] = 1;
			}

			if((x-i) >= 0 && (y+i) < MASU)
			{
				if(game[x-i][y+i] == -stone) direct[5] = 1;
			}

			if((x-i) >= 0)
			{
				if(game[x-i][y] == -stone) direct[6] = 1;
			}

			if((x-i) >= 0 && (y-i) >= 0)
			{
				if(game[x-i][y-i] == -stone) direct[7] = 1;
			}

			if((x+i) < MASU && (y+i) < MASU)
			{
				if(game[x+i][y+i] == -stone) direct[3] = 1;
			}

			boolean flag = false;

			//(y-i) >= 0

			for (i = 2; i <= MASU; i++) 
			{
				if((x+i) < MASU)
				{	
					if(game[x+i][y] == stone && direct[2] == 1)
					{
						flag = true;//同じ色の石で続いていたら
						direct[2] = 2;
					}
					else if(game[x+i][y] == -stone && direct[2] == 1) direct[2] = 1;
					else if(direct[2] == 2) flag = true;
					else direct[2] = 0;
				}

				if((x+i) < MASU && (y-i) >= 0)
				{
					//右上
					if(game[x+i][y-i] == stone && direct[1] == 1)
					{
						flag = true;//同じ色の石で続いていたら
						direct[1] = 2;
					}
					else if(game[x+i][y-i] == -stone && direct[1] == 1) direct[1] = 1;//同じ色の石が続いていたら
					else if(direct[1] == 2) flag = true;
					else direct[1] = 0;//ブランクもしくはもう既にブランクがある場合
				}

				if((y-i) >= 0)
				{
					//上
					if(game[x][y-i] == stone && direct[0] == 1)
					{
						flag = true;
						direct[0] = 2;
					}
					else if(game[x][y-i] == -stone && direct[0] == 1) direct[0] = 1;
					else if(direct[0] == 2) flag = true;
					else direct[0] = 0;
				}

				//(y+i) < MASU

				if((x-i) >= 0)
				{
					if(game[x-i][y] == stone && direct[6] == 1){
						flag = true;//同じ色の石で続いていたら
						direct[6] = 2;
					}
					else if(game[x-i][y] == -stone && direct[6] == 1) direct[6] = 1;
					else if(direct[6] == 2) flag = true;
					else direct[6] = 0;
				}

				if((x-i) >= 0 && (y+i) < MASU)
				{
					if(game[x-i][y+i] == stone && direct[5] == 1){
						flag = true;//同じ色の石で続いていたら
						direct[5] = 2;
					}
					else if(game[x-i][y+i] == -stone && direct[5] == 1) direct[5] = 1;
					else if(direct[5] == 2) flag = true;
					else direct[5] = 0;
				}

				if((y+i) < MASU)
				{
					if(game[x][y+i] == stone && direct[4] == 1){
						flag = true;//同じ色の石で続いていたら
						direct[4] = 2;						
					}
					else if(game[x][y+i] == -stone && direct[4] == 1) direct[4] = 1;
					else if(direct[4] == 2) flag = true;
					else direct[4] = 0;
				}

				if((x-i) >= 0 && (y-i) >= 0)
				{
					if(game[x-i][y-i] == stone && direct[7] == 1){
						flag = true;//同じ色の石で続いていたら
						direct[7] = 2;
					}
					else if (game[x-i][y-i] == -stone && direct[7] == 1) direct[7] = 1;
					else if(direct[7] == 2) flag = true;
					else direct[7] = 0;
				}

				if((x+i) < MASU && (y+i) < MASU)
				{
					if(game[x+i][y+i] == stone && direct[3] == 1){
						flag = true;//同じ色の石で続いていたら
						direct[3] = 2;
					}
					else if (game[x+i][y+i] == -stone && direct[3] == 1) direct[3] = 1;
					else if(direct[3] == 2) flag = true;
					else direct[3] = 0;
				}
			}
		return flag;
		}
	}

	public void reverse(int x, int y, int stone)
	{
		if(direct == null){
			return;
		}

		for (int s = 0; s < MASU; s++) {
			System.out.println(s +""+direct[s]);
		}

		int i = 1;

		if(direct[0] == 2)
		{
			while(game[x][y-i] == -stone)
			{
				game[x][y-i] = stone;
				update(getGraphics());
				infopanel.update(countBlack(), countWhite(), flagblack);
				sleep(REVERSE_SLEEP);
				i++;
			}
			i=1;
		}

		if(direct[1] == 2)
		{
			while(game[x+i][y-i] == -stone)
			{
				game[x+i][y-i] = stone;
				update(getGraphics());
				infopanel.update(countBlack(), countWhite(), flagblack);
				sleep(REVERSE_SLEEP);
				i++;
			}
			i=1;
		}

		if(direct[2] == 2)
		{
			while(game[x+i][y] == -stone)
			{
				game[x+i][y] = stone;
				update(getGraphics());
				infopanel.update(countBlack(), countWhite(), flagblack);
				sleep(REVERSE_SLEEP);
				i++;
			}
			i=1;
		}

		if(direct[3] == 2)
		{
			while(game[x+i][y+i] == -stone)
			{
				game[x+i][y+i] = stone;
				update(getGraphics());
				infopanel.update(countBlack(), countWhite(), flagblack);
				sleep(REVERSE_SLEEP);
				i++;
			}
			i=1;
		}

		if(direct[4] == 2)
		{
			while(game[x][y+i] == -stone)
			{
				game[x][y+i] = stone;
				update(getGraphics());
				infopanel.update(countBlack(), countWhite(), flagblack);
				sleep(REVERSE_SLEEP);				
				i++;
			}
			i=1;
		}

		if(direct[5] == 2)
		{
			while(game[x-i][y+i] == -stone)
			{
				game[x-i][y+i] = stone;
				update(getGraphics());
				infopanel.update(countBlack(), countWhite(), flagblack);
				sleep(REVERSE_SLEEP);
				i++;
			}
			i=1;
		}

		if(direct[6] == 2)
		{
			while(game[x-i][y] == -stone)
			{
				game[x-i][y] = stone;
				update(getGraphics());
				infopanel.update(countBlack(), countWhite(), flagblack);
				sleep(REVERSE_SLEEP);
				i++;
			}
			i=1;
		}

		if(direct[7] == 2)
		{
			while(game[x-i][y-i] == -stone)
			{
				game[x-i][y-i] = stone;
				update(getGraphics());
				infopanel.update(countBlack(), countWhite(), flagblack);
				sleep(REVERSE_SLEEP);
				i++;
			}
			i=1;
		}

		direct = null;
	}

	private void sleep(int x)
	{
		try {
            Thread.sleep(x);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
	}

	public int countcanplace(int stone)
	{
		int result = 0;
		for (int x = 0; x < MASU; x++) {
			for (int y = 0; y < MASU; y++) {
				if(canPut(x,y,stone))
				{
					result++;
				}
			}
		}

		return result;
	}

	public void pass()
	{
		flagblack = !flagblack;
		infopanel.update(countBlack(), countWhite(), flagblack);
	}

	//黒の個数
	public int countBlack()
	{
		int x = 0;
		for (int i = 0; i < MASU; i++) {
			for (int s = 0; s < MASU; s++) {

				if(game[i][s] == BLACK)
				{
					x++;
				}
			}
		}

		return x;
	}

	//白の個数
	public int countWhite()
	{
		int y = 0;

		for (int i = 0; i < MASU; i++) {
			for (int s = 0; s < MASU; s++) {
				if(game[i][s] == WHITE)
				{
					y++;
				}
			}
		}

		return y;
	}

	public void paintComponent(Graphics g) {
        super.paintComponent(g);

        // 盤面を描く
        drawBoard(g);
        drawStone(g);
    }

    public void drawBoard(Graphics g)
    {
    	g.setColor(new Color(0, 128, 128));//緑に設定
    	g.fillRect(0, 0, WIDTH, HEIGHT);

    	g.setColor(Color.BLACK);

    	for (int i = 1; i < MASU; i++) {
    		g.drawLine(i * SIZE, 1, i * SIZE, HEIGHT);
    	}

    	for (int j = 1; j < MASU; j++) {
    		g.drawLine(0, j*SIZE, WIDTH, j*SIZE);
    	}

    	g.drawRect(0,0,WIDTH,HEIGHT);
    }

    public void drawStone(Graphics g)
    {
    	for(int i = 0; i < MASU; i++)
    	{
    		for (int j = 0; j < MASU; j++) 
    		{
    			if(game[i][j] == BLANK)
    			{
    				continue;
    			}

    			if(game[i][j] == BLACK)
    			{
    				g.setColor(Color.BLACK);
    			}else
    			{
    				g.setColor(Color.WHITE);
    			}

    			g.fillOval(i * SIZE + 6, j * SIZE + 6, SIZE - 12, SIZE - 12);
    		}
    	}
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }
}
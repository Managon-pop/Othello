import java.awt.*;
import javax.swing.*;

public class InfoPanel extends JPanel
{
	public JLabel black;
	public JLabel white;
	public JLabel black_count;
	public JLabel white_count;
	public JLabel turn;

	public InfoPanel()
	{
		setLayout(null);
		black = new JLabel();
		black.setBounds(0,0,20,10);
		white = new JLabel();
		white.setBounds(0,30,20,10);

		black_count = new JLabel("2");
		black_count.setBounds(30,0,100,10);
		white_count = new JLabel("2");
		white_count.setBounds(30,60,100,10);

		turn = new JLabel("あなたの番です");
		turn.setBounds(0,150,200,20);

		add(black);
		add(white);
		add(black_count);
		add(white_count);
		add(turn);
	}

	public  void update(int black, int white, boolean playerturn)
	{
		black_count.setText(""+black);
		white_count.setText(""+white);
		if(playerturn)
		{
			turn.setText("あなたの番です");
		}else
		{
			turn.setText("コンピューターが思考中...");
		}
	}
}
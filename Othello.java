import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class Othello extends JFrame
{
	public Othello()
	{
		setTitle("Othello");
		setLayout(null);
		setResizable(false);
		setSize(800,600);//大きさ
		setLocationRelativeTo(null);//画面の中央に設置

		InfoPanel infopanel = new InfoPanel();
		infopanel.setBounds(580, 100, 200,GamePanel.HEIGHT);
		add(infopanel);
		
		GamePanel gamepanel = new GamePanel(infopanel);
		gamepanel.setBounds(30,30,GamePanel.WIDTH,GamePanel.HEIGHT);
		add(gamepanel);
	}

	public static void main(String[] args)
	{
		Othello frame = new Othello();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}